const stripTrailingSlash = (url: string) => {
  if (url.substr(-1) === "/") {
    return url.substr(0, url.length - 1);
  }

  return url;
};

export default stripTrailingSlash;
