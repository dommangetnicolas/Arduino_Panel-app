import { StyleSheet } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import colors from "../../constants/colors";
import fonts from "../../constants/fonts";

export default StyleSheet.create({
  textInput: {
    paddingVertical: RFValue(9),
    paddingHorizontal: RFValue(13),
    borderRadius: 5,
    borderColor: colors.white,
    borderWidth: 1,
    backgroundColor: "#fff",
    color: "#000",

    fontSize: RFValue(14),
    fontFamily: fonts.primary,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  textInputInvalid: {
    borderColor: colors.danger,
    borderWidth: 1,
  },
});
