import { StyleSheet } from "react-native";
import colors from "../../constants/colors";
import { RFValue } from "react-native-responsive-fontsize";
import fonts from "../../constants/fonts";

export default StyleSheet.create({
  button: {
    paddingVertical: RFValue(10),
    paddingHorizontal: RFValue(25),
    borderRadius: RFValue(5),
    backgroundColor: colors.primary,
    color: "#000",

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
    alignSelf: "flex-start",
  },
  text: {
    color: colors.white,
    fontSize: RFValue(14),
    fontFamily: fonts.primaryBold,
    
  },
});
