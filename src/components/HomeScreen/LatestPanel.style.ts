import { StyleSheet } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import fonts from "../../constants/fonts";

export default StyleSheet.create({
  container: {
    marginBottom: RFValue(10),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  title: {
    fontSize: RFValue(20),
    fontFamily: fonts.primarySemiBold,
    marginBottom: RFValue(15),
  },
  line1: {
    fontSize: RFValue(18),
    fontFamily: fonts.primarySemiBold,
    opacity: 0.8,
  },
  line2: {
    fontSize: RFValue(16),
    fontFamily: fonts.primarySemiBold,
    opacity: 0.6,
  },
  color: {
    height: RFValue(38),
    width: RFValue(38),
    backgroundColor: "#007FFF",
    borderRadius: RFValue(5),
  },
});
