import { StyleSheet } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import fonts from "../../constants/fonts";

export default StyleSheet.create({
  title: {
    fontSize: RFValue(20),
    fontFamily: fonts.primaryBold,
    marginBottom: RFValue(20),
  },
  input: {
    marginBottom: RFValue(10),
  },
  button: {
    alignSelf: "flex-end",
  },
  colorContainer: {
    flexDirection: "row",
    height: RFValue(38),
    marginBottom: RFValue(10),
  },
  colorSelected: {
    opacity: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  colorPreview: {
    flex: 1,
    opacity: 0.8,
    marginRight: RFValue(10),
    borderRadius: RFValue(5),
    backgroundColor: "red",
    aspectRatio: 1,
  },
});
