import React, { Children, FunctionComponent } from "react";
import Svg, { Path, G } from "react-native-svg";
import { View, SafeAreaView, ScrollView } from "react-native";
import styles from "./HomeBackground.style";
import Header from "./Header";

const Background: FunctionComponent = (props) => {
  const { children } = props;

  return (
    <View style={styles.container}>
      <View style={styles.backgroundContainer}>
        <Svg
          width="100%"
          height="30%"
          style={styles.svg}
          viewBox="0 0 375 200"
          preserveAspectRatio="none"
        >
          <Path
            d="M0 0h375v170c0 16.569-13.431 30-30 30H30c-16.569 0-30-13.431-30-30V0z"
            fill="#0984E3"
            fillRule="evenodd"
          />
        </Svg>

        <Svg
          width="100%"
          height="28%"
          viewBox="0 0 375 187"
          style={styles.svg}
          preserveAspectRatio="none"
        >
          <Path
            d="M-113 192l11.624-5.3c11.624-5.7 34.804-15.7 58.187-37.4C-20.043 128 3.24 96 26.283 112c23.384 16 46.767 80 69.811 74.7 23.282-5.7 46.428-79.7 69.812-117.4 23.18-37.3 46.427-37.3 69.81-16 23.079 21.7 46.428 63.7 69.473 69.4 23.35 5.3 46.428-26.7 58.289-42.7L375 64V0h-488v192z"
            fill="#09F"
            fillRule="nonzero"
          />
        </Svg>
      </View>

      <SafeAreaView style={{ flex: 1 }}>
        <Header />
        <ScrollView contentContainerStyle={styles.contentContainer}>{children}</ScrollView>
      </SafeAreaView>
    </View>
  );
};

export default Background;
