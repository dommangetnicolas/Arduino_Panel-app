import React, { FunctionComponent } from "react";
import { Card, Text } from "../ui";
import { View, TouchableOpacity } from "react-native";
import styles from "./LatestPanel.style";
import { Panel } from "../../store/panels/types";
import { useDispatch } from "react-redux";
import { setNewPanel } from "../../store/panels/actions";

type Props = {
  panel: Panel;
};

const LatestPanel: FunctionComponent<Props> = (props) => {
  const {
    panel,
    panel: { line1, line2, color },
  } = props;

  const dispatch = useDispatch();

  const onPress = () => {
    dispatch(setNewPanel(panel));
  };

  return (
    <TouchableOpacity onPress={onPress}>
      <Card style={styles.container}>
        <View>
          <Text style={styles.line1}>{line1}</Text>
          <Text style={styles.line2}>{line2}</Text>
        </View>
        <View style={[styles.color, { backgroundColor: color }]} />
      </Card>
    </TouchableOpacity>
  );
};

export default LatestPanel;
