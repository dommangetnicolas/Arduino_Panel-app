import { SET_API_URL, Panel, ADD_PANEL, SET_NEW_PANEL } from "./types";

export const setApiUrl = (apiUrl: string) => ({
  type: SET_API_URL,
  apiUrl,
});

export const addPanel = (panel: Panel, limit: number = 3) => ({
  type: ADD_PANEL,
  panel,
  limit,
});

export const setNewPanel = (newPanel: Panel) => ({
  type: SET_NEW_PANEL,
  newPanel,
});
