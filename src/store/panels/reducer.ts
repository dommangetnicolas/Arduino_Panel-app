import {
  ADD_PANEL,
  PanelsActionTypes,
  PanelsState,
  SET_API_URL,
  SET_NEW_PANEL,
} from "./types";

const initialState: PanelsState = {
  newPanel: null,
  panels: [],
  apiUrl: "",
};

const reducer = (state = initialState, action: PanelsActionTypes) => {
  let newPanels = [...state.panels];

  switch (action.type) {
    case SET_API_URL:
      return { ...state, apiUrl: action.apiUrl };

    case ADD_PANEL:
      newPanels = newPanels.slice(0, action.limit - 1);
      newPanels.unshift(action.panel);

      return { ...state, panels: newPanels };

    case SET_NEW_PANEL:
      return {...state, newPanel: action.newPanel};

    default:
      return state;
  }
};

export default reducer;
