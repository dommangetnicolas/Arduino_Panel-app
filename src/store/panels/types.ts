export const SET_API_URL = "SET_API_URL";
export const ADD_PANEL = "ADD_PANEL";
export const SET_NEW_PANEL = "SET_NEW_PANEL";

export interface Panel {
  line1: string;
  line2: string;
  color: string;
}

export interface PanelsState {
  newPane: Panel,
  apitUrl: string;
  panels: Array<Panel>;
}

export interface SetApiUrlAction {
  type: typeof SET_API_URL;
  apiUrl: string;
}

export interface AddPanelAction {
  type: typeof ADD_PANEL;
  panel: Panel,
  limit: number,
}

export interface SetNewPanelAction {
  type: typeof SET_NEW_PANEL;
  newPanel: Panel,
}

export type PanelsActionTypes = SetApiUrlAction | AddPanelAction | SetNewPanelAction;
