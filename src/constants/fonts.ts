export default {
  primary: "cabin-regular",
  primarySemiBold: "cabin-semibold",
  primaryBold: "cabin-bold",
};
