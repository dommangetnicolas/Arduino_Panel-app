export default {
  primary: "#0984E3",
  danger: "#e74c3c",
  white: "#FFFFFF",
  screenBackground: "#F4F3F8",
};
