import React from "react";
import HomeBackground from "../components/HomeScreen/HomeBackground";
import NewPanel from "../components/HomeScreen/NewPanel";
import LatestPanels from "../containers/HomeScreen/LatestPanels";

const HomeScreen = () => {
  return (
    <HomeBackground>
      <NewPanel />
      <LatestPanels />
    </HomeBackground>
  );
};

export default HomeScreen;
