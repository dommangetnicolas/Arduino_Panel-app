import React, { useState } from "react";
import { Text, View } from "react-native";
import LatestPanel from "../../components/HomeScreen/LatestPanel";
import styles from "./LatestPanels.style";
import { useSelector, RootStateOrAny } from "react-redux";
import { Panel } from "../../store/panels/types";

const LatestPanels = () => {
  const panels = useSelector(({ Panels }: RootStateOrAny) => Panels.panels);

  if (!panels.length) {
    return null;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Latest</Text>
      {panels.map((panel: Panel, key: string) => (
        <LatestPanel key={key} panel={panel} />
      ))}
    </View>
  );
};

export default LatestPanels;
